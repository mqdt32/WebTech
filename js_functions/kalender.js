const   datum = new Date(),
        datum_monat = datum.getMonth() +1,
        datum_jahr = datum.getFullYear();

Kalender(datum_monat, datum_jahr, 'calendar');

function Kalender (Monat, Jahr, Kalender_id) {
    const   monatsname    =  ["Januar", "Februar", "März", "April", "Mai", "Juni",
                            "Juli", "August", "September", "Oktober", "November", "Dezember"],
            tagname       = ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"];


    // Nun folgt das aktuelle Datum

    const aktuell = new Date();
    // Erstmal keine Hervorhebung, da -1 nicht im Kalender erscheint
    let aktuellerTag = -1;
    if ((aktuell.getFullYear() == Jahr) && (aktuell.getMonth() + 1 == Monat)) {
        aktuellerTag = aktuell.getDate();
    }

    // Nun wird der Wochentag des ersten Tags im Monat ermittelt

    const Zeit = new Date(Jahr, Monat - 1, 1);
    const Start = (Zeit.getDay() + 6) % 7;

    // Anzahl der Tage im Monat
    // Monate haben nicht mehr als 31 Tage
    let ende = 31;

    // Monate behandeln, die nicht 31 Tage haben

    if (Monat == 4 || Monat == 6 || Monat == 9 || Monat == 11) --ende;

    if (Monat == 2) {
    ende = ende - 3;
    // Februar hat in Schaltjahren 29 Tage
    if (Jahr %   4 == 0) Stop++;
    if (Jahr % 100 == 0) Stop--;
    if (Jahr % 400 == 0) Stop++;
    }

    // Tabelle wird bearbeitet

    const tabelle = document.getElementById('calendar');
    if (!tabelle) return false;

    // Tabelle Header

    let monatszeile = monatsname[Monat - 1] + " " + Jahr,
        caption = tabelle.createCaption();
        caption.innerHTML = monatszeile;

    // Tabelle Head

    let zeile = tabelle.insertRow(0);
    for (let i = 0; i<7; i++) {
        let cell = row.insertCell(i);
        cell.innerHTML = tagname[i];
    }

    // Tag ermitteln

    let tagnumber = 1;

    // Monate gehen über 4-6 Wochen
    // Über Tabelle iterieren
    for (let i = 0; tagnumber <= ende; i++) {
        let row = tabelle.insertRow(1 + i);
        for (let j = 0; j < 7; j++) {
        let cell = row.insertCell(j);
            // Zellen vor dem Start und Zeilen nach dem Ende werden leer
            if (((i == 0) && (j < Start)) || (tagnumber > ende)) {
                cell.innerHTML = ' ';
            }
            else {
                // Zellen mit Tag befüllen
                cell.innerHTML = tagnumber;
                cell.className = 'tag';
                //  aktueller Tag mit Klasse heute markiert
                if (tagnumber == aktuellerTag) {
                    cell.className = cell.className + 'aktuell';
                }
                tagnumber++;
            }
        }
    }
    return true;
}

    