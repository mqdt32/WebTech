let includes = document.getElementsByTagName('include');
for(var i=0; i<includes.length; i++){
    let include = includes[i];
    load_file(includes[i].attributes.src.value, function(text){
        include.insertAdjacentHTML('afterend', text);
        include.remove();
    });
}
function load_file(filename, callback) {
    fetch(filename).then(response => response.text()).then(text => callback(text));
}



function toggleButton(x) {
    x.classList.toggle("change");
    openDropdown();
}


function openDropdown() {
    document.getElementById("DropdownMenu").classList.toggle("show")
    if (event.target.matches(".container")) {
        var dropdowns = document.getElementsByClassName("container");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i]
            if (openDropdown.classList.contains("show")) {
                openDropdown.classList.remove("show");
            }
        }
    }
}


// window.onclick = function (event) {
//     if (!event.target.matches(".container")) {
//         var dropdowns = document.getElementsByClassName("dropdown-content");
//         var i;
//         for (i = 0; i < dropdowns.length; i++) {
//             var openDropdown = dropdowns[i]
//             if (openDropdown.classList.contains("show")) {
//                 openDropdown.classList.remove("show");
//             }
//         }
//     }
//     // TODO: Change button style when closing menu
//     // toggleButton(document.getElementById("MenuButton"))
// }