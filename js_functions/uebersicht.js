function angeboteAuswaehlen() {
    var offer1price = 0;
    var offer2price = 0;
    var offer3price = 0;
    var roomprice = 0;
    var date1 = new Date(sessionStorage.getItem('D'));
    var date2 = new Date(sessionStorage.getItem('D2'));        
    var reisedauer = Math.floor((date2-date1)/(24*3600*1000)); // Wenn die Dauer noch mit dem Preis aus der DB verknüpft wird kann hier einfach der Preis berechnet werden.
    var checkBox1 = document.getElementById("cb1");
    if (checkBox1.checked == true){
        sessionStorage.setItem('Angebot5', 'Fitness');
        offer1price = 9 * reisedauer;
    } else {
        sessionStorage.setItem('Angebot5', '');
    }
    var checkBox2 = document.getElementById("cb2");
    if (checkBox2.checked == true){
        sessionStorage.setItem('Angebot6', 'Gepäckservice');
        offer2price = 39;
    } else {
        sessionStorage.setItem('Angebot6', '');
    }
    var checkBox3 = document.getElementById("cb3");
    if (checkBox3.checked == true){
        sessionStorage.setItem('Angebot7', 'Vollpension');
        offer3price = 19 * reisedauer;
    } else {
        sessionStorage.setItem('Angebot7', '');
    }
    var offerprice = offer1price + offer2price + offer3price;
    if (sessionStorage.getItem('Zimmer') == 'Zimmer 1') {
        roomprice = 100;
    }
    if (sessionStorage.getItem('Zimmer') == 'Zimmer 2') {
        roomprice = 200;
    }
    if (sessionStorage.getItem('Zimmer') == 'Zimmer 3') {
        roomprice = 300;
    }
    sessionStorage.setItem('Preis', offerprice + roomprice);
    sessionStorage.setItem('rdauer', reisedauer)
    }