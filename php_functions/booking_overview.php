<?php

$conn = mysqli_connect("localhost", "root", "passwort", "webtech");

if (!$conn) {

    die("Connection failed: " . mysqli_connect_error());

} else {

    $sql = "SELECT b.bookingID, b.roomID, b.arrival, b.departure, c.first_name, c.last_name FROM booking AS b
            INNER JOIN customer c on b.customerID = c.customerID";
    $result = $conn -> query($sql);

    require "booking_overview.view.php";
    $conn -> close();

}
