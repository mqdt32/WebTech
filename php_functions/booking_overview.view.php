<!DOCTYPE html>

<html lang="de">

    <head>

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="../stylesheets/booking_overview.css">

        <title>Buchungsübersicht</title>

    </head>


    <body>

        <?php include "menu.php"; ?>

        <div class="scroll">
            <h1 id="header">Buchungsübersicht</h1>
            <a class="back" href="../index.html">
                <img src="../images/arrow_back.svg" alt="Go back">
            </a>
        </div>

        <div class="booking_overview">
            <table class="overview_table">
                <tr>
                    <th>Kunde</th>
                    <th>Zimmer</th>
                    <th>Anreise</th>
                    <th>Abreise</th>
                    <th>Gebuchte Angebote</th>
                </tr>

                <?php
                    if ($result -> num_rows > 0) {
                        while ($row = mysqli_fetch_object($result)) {
                            $inner_sql = "SELECT * FROM booked_offers AS bo
                                          INNER JOIN offers o on bo.offerID = o.offerID
                                          WHERE bookingID =" . mysqli_real_escape_string($conn, $row -> bookingID);
                            $inner_result = $conn -> query($inner_sql);
                            $booked_offers = "";
                            while ($row2 = mysqli_fetch_object($inner_result)) {
                                if ($booked_offers <> "") {
                                    $booked_offers .= ", ";
                                }
                                $booked_offers .= $row2 -> offer_name;
                            }
                            ?>
                            <tr>
                                <td><?php echo $row -> last_name . ", " . $row -> first_name; ?></td>
                                <td><?php echo $row -> roomID; ?></td>
                                <td><?php echo $row -> arrival; ?></td>
                                <td><?php echo $row -> departure; ?></td>
                                <td><?php echo $booked_offers ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <th colspan="2">Keine Buchungen gefunden!</th>
                        </tr>
                        <?php
                    }
                ?>

            </table>
        </div>

    </body>

</html>