<!DOCTYPE html>

<html lang="de">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Login</title>

        <link rel="stylesheet" href="../stylesheets/login.css">

    </head>


    <body >

        <?php include "menu.php"; ?>
        <?php $status = ""; ?>

        <script>
            function call_alert(msg) {
                document.getElementById("login-error-msg").innerHTML = msg;
            }
        </script>

        <div class="scroll">
            <a class="back" href="../index.html">
                <img src="../images/arrow_back.svg" alt="Go back">
            </a>
        </div>

        <main id="main-holder">

            <h1 id="login-header">Anmeldung</h1>

            <div id="login-error-msg-holder">
                <p id="login-error-msg"></p>
            </div>

            <form id="login-form" action="login.php" method="POST">
                <input type="text" name="username" id="username-field" class="login-form-field" placeholder="Benutzername">
                <input type="password" name="password" id="password-field" class="login-form-field" placeholder="Passwort">
                <input type="submit" name="submit" value="Anmelden" id="login-form-submit">
            </form>
        </main>
    </body>

</html>