<!DOCTYPE html>

<html lang="de">

    <head>
        <meta charset="UTF-8">
        <title>Menu</title>

        <link rel="stylesheet" href="../stylesheets/menu_button.css">

        <script src="../js_functions/menu_functions.js"></script>

    </head>


    <body>

        <div id="MenuButton" class="container" onclick="toggleButton(this)">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
        </div>

        <div id="DropdownMenu" class="dropdown-menu">
            <div id="DropdownContent" class="dropdown-content">
                <a id="t1" href="../index.html">Home</a>
                <a id="t2" href="../rooms.html">Zimmer</a>
                <a id="t3" href="../offers.html">Angebote</a>
                <a id="t4" href="../imprint.html">Imperessum</a>
            </div>
            <div class="login-button">
                <button onclick="window.location.href='login.view.php'">Anmeldung</button>
            </div>
        </div>

    </body>

</html>