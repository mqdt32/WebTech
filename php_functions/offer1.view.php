<!DOCTYPE html>

<html lang="de">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../stylesheets/offer1.css">
        <title>Angebot 1</title>
    </head>


    <body>

        <?php include "menu.php"; ?>

        <div class="scroll">
            <h1 id="u_schrift">
                Angebot 1
            </h1>
            <a class="b2" href="../offers.html">
                <img src="../images/arrow_back.svg" alt="Go back">
            </a>
        </div>

        <img class="bv" src="../images/fitness_voschau.jpg"/>

        <div class="normaler_Text">
            <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
                ea rebum.
                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua.
            </p>
            <p>
                At vero eos et accusam et justo duo dolores et ea rebum.
            </p>
            <p>
                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita
                kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
            </p>
            <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
                ea rebum.
                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
            </p>
        </div>

        <div class="zimmertabelle_cont">
            <table class="zimmer1tab">
                <tr>
                    <th>Reservierbar</th>
                    <th>Zubuchbar zu:</th>
                    <th>Preis</th>
                </tr>
                <tr>
                    <td>Ja</td>
                    <td>Den Zimmern</td>
                    <td><?= $rent ?> €</td>
                </tr>
            </table>
        </div>
        <br>
    </body>

</html>