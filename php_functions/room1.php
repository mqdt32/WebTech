<?php
$conn = mysqli_connect("localhost","root","passwort","webtech");


if (!$conn) {

    die("Connection failed: " . mysqli_connect_error());

} else {

    $sql = "SELECT * FROM room WHERE roomID = 1";
    $result = $conn->query($sql);

    while ($obj = mysqli_fetch_object($result)) {
        $rent = $obj->rent;
        $room_size = $obj->room_size;
        $beds_quantity = $obj->beds_quantity;
    }

    require "room1.view.php";
    $conn -> close();

}