<!DOCTYPE html>

<html lang="de">

    <head>

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/stylesheets/room1.css">
        <title>Zimmer 3</title>

    </head>


    <body>

        <?php include "menu.php"; ?>

        <div class="scroll">
            <h1 id="u_schrift">
                Zimmer 3
            </h1>
            <a class="b2" href="/rooms.html">
                <img src="/images/arrow_back.svg" alt="Go back">
            </a>

        </div>

        <img class="bv" src="/images/zimmern3.jpg"/>

        <div class="normaler_Text">
            <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
                dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                erat, sed diam voluptua.
            </p>
            <p>
                At vero eos et accusam et justo duo dolores et ea rebum.
            </p>
            <p>
                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                no sea takimata sanctus est Lorem ipsum dolor sit amet.
            </p>
            <p>
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
                dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
                erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
            </p>
        </div>

        <div class="zimmertabelle_cont">
            <table class="zimmer1tab">
                <tr>
                    <th>Größe</th>
                    <th>Betten</th>
                    <th>Preis</th>
                </tr>
                <tr>
                    <td><?= $room_size ?> m²</td>
                    <td><?= $beds_quantity ?> Betten</td>
                    <td><?= $rent ?> €</td>
                </tr>
            </table>
        </div>

        <div class="b1">
            <button>Verfügbarkeit prüfen</button>
        </div>
        <br>

    </body>

</html>